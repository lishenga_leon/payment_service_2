from django.urls import include, path
from MPESA.logic import mpesa
from MPESA.encryption import mpesa_encrypt

urlpatterns = [
 
    #Authenticate routes
    path('mpesa/PushStkc2b/', mpesa.PushStkc2b),
    path('mpesa/accountBalance/', mpesa.accountBalance),
    path('mpesa/transactionStatus/', mpesa.transactionStatus),
    path('mpesa/reversalMpesaTransaction/', mpesa.reversalMpesa),
    path('mpesa/b2c/', mpesa.b2c),

    path('mpesa/encrypt/', mpesa_encrypt.encryptInitiatorPassword),
]