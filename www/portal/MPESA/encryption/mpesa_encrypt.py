from Crypto.PublicKey import RSA
from base64 import b64encode
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.conf import settings
import os

path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
MPESA_PASSWORD = settings.MPESA_PASSWORD 
NAME = 'sandbox.cer'

"""
Encrypt the certificate to authenticate Mpesa Transactions
"""
def encryptInitiatorPassword():
    cert_file = open(find(NAME, path), 'r')
    cert_data = cert_file.read() 
    cert_file.close()

    cert = RSA.import_key(cert_data, passphrase=MPESA_PASSWORD)
    cipher = cert.publickey().export_key()

    return b64encode(cipher)

"""
Search for the certificate in the project
"""
def find(NAME, path):
    for root, dirs, files in os.walk(path):
        if NAME in files:
            return os.path.join(root, NAME)

