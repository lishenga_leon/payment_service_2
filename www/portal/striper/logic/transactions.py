from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
import datetime
from django.core.paginator import Paginator
import stripe
from django.conf import settings


STRIPE_SECRET_KEY = settings.STRIPE_SECRET_KEY

#initiate a card charge via stripe on a customer card
@api_view(['POST'])
def create_stripe_customer_charge(request): 
    """
    Charge Card for Stripe check out

    -----
        {
            stripe_id:jhadha,
            amount:50,
            currency:USD
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST': 
            
            stripe.api_key = STRIPE_SECRET_KEY

            stripe_charge = stripe.Charge.create(
                amount = request.data['amount'],
                currency = request.data['currency'],
                customer = request.data['stripe_id'],
                description="Payment for a service",
            )

            if stripe_charge['status'] == "succeeded":
                success={
                    'message':'success',
                    "data":stripe_charge,
                    'status_code':200
                }
                return Response(success)

            else:
                error={
                    'message':'could not charge account',
                    'data':[],
                    'status_code':500
                }

                return Response(error) 


    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error)  


#Get the stripe balance for the stripe account
@api_view(['GET'])
def get_stripe_balance(request): 

    try:
        if request.method == 'POST':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'GET':   
            stripe.api_key = STRIPE_SECRET_KEY

            balance=stripe.Balance.retrieve()
            success={
                "data":balance,
                "message":"success",
                "status_code":200
                } 
            return Response(success) 

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)
        

#Check person stripe details
@api_view(['POST'])
def check_person_stripe_details(request): 
    """
    Check person stripe details
    -----
        {
            stripe_id:cus_asfhajho13
        }
    """

    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':   
            stripe.api_key = STRIPE_SECRET_KEY

            details=stripe.Customer.retrieve(request.data['stripe_id'])
            success={
                "data":details,
                "message":"success",
                "status_code":200
                } 
            return Response(success) 

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)

#initiate a stripe payout to a specific bank linked to the stripe account.
@api_view(['POST'])
def stripe_payout(request): 
    """
    Create Stripe Payout

    -----
        {
            amount:50,
            currency:USD,
            accountId_or_cardNo: 3434545,
        }
      
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':  
            stripe.api_key = STRIPE_SECRET_KEY

            payout=stripe.Payout.create(
                amount=request.data['amount'],
                currency=request.data['currency'],
                destination=request.data['accountId_or_cardNo'],
            )
            success={
                "data":payout,
                "message":"success",
                "status_code":200
                } 
            return Response(success)   

    except BaseException as e:

        error={
            'status_code':500,
            'message':'error:'+ str(e),
            'data':{}
        }
        return Response(error)
