from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
import datetime
from django.core.paginator import Paginator
import stripe
from django.conf import settings


STRIPE_SECRET_KEY = settings.STRIPE_SECRET_KEY

#create a stripe user to permit payment
@api_view(['POST'])
def create_stripe_user(request): 
    """
    Create a stripe user to permit payment

    -----
        {
            email:lishengaleon@gmail.com,
        }
      
    """
    try:
        my_user = stripe.Customer.create(email= request.data['email'], api_key=STRIPE_SECRET_KEY)
        success={
            "data":my_user,
            "message":"success",
            "status_code":200
        } 

        return Response(success) 

    except BaseException as e:
        
        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error)  


#add existing person card   
@api_view(['POST'])
def add_person_card(request):    
    """
    Create Stripe Person Card
    -----
        {
            stripe_id:jdsjkds,
            number:424242424242424,
            exp_month:07,
            exp_year:22,
            cvc:494,
        }

    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':
            card = {
                "number": request.data['number'] ,
                "exp_month": request.data['exp_month'],
                "exp_year": request.data['exp_year'],
                "cvc": request.data['cvc']
            }
            stripe.api_key = STRIPE_SECRET_KEY
            token = stripe.Token.create(card=card, api_key=STRIPE_SECRET_KEY)  
            striper = stripe.Customer.retrieve(request.data['stripe_id'])
            create_card=striper.sources.create(source= token['id'])
            success={
                "data":create_card,
                "message":"success",
                "status_code":200
            } 
            return Response(success)  

    except BaseException as e:

        error={
            'data':[],
            'message':'error'+ str(e),
            'status_code':500
        }

        return Response(error) 


#Display all the customer cards for a user
@api_view(['POST'])
def get_customer_cards(request):
    """
    Display all the customer cards for a user
    -----
        {
            stripe_id:cus_asfhajho13
        }
    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':  
            stripe.api_key = STRIPE_SECRET_KEY
            card_info = stripe.Customer.retrieve(request.data['stripe_id']).sources.list(limit=1, object='card')
            success={
                'data':card_info,
                'status_code':200,

            }
            return Response(success)

    except BaseException as e:
        error={
            'status_code':500,
            'message':'error' + str(e),
            'data':{
               
            }
        }
        return Response(error)

# delete a specific person for stripe
@api_view(['DELETE'])
def delete_stripe_person(request):
    """
    remove person
    -----
        {
            stripe_id:hkdjlew
        }

    """
    try:
        if request.method == 'DELETE':
            _id = request.data['stripe_id']
            stripe.api_key = STRIPE_SECRET_KEY
            cu = stripe.Customer.retrieve(_id)
            delete_stripe = cu.delete()
            data = {
                "message": 'Person deleted',
                "status_code": 200
            }
            return Response(data)
        else:
            snippets = {
                'message': "invalid request",
                "status_code": 401
            }
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

    except BaseException as e:

        error = {
            'status_code': 500,
            'message': 'error'+str(e),
        }
        return Response(error)

# Get all customers for the application
@api_view(['POST'])
def get_all_customers(request):
    """
    Get all customers
    -----
        {
            limit: int
        }

    """
    try:
        if request.method == 'GET':
            snippets='success'
            return Response(snippets, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'POST':  
            stripe.api_key = STRIPE_SECRET_KEY
            customers = stripe.Customer.list(limit=request.data['limit'])
            success={
                'data':customers,
                'status_code':200,
            }
            return Response(success)

    except BaseException as e:
        error={
            'status_code':500,
            'message':'error' + str(e),
            'data':{
               
            }
        }
        return Response(error)