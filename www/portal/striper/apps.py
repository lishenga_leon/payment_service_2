from django.apps import AppConfig


class StriperConfig(AppConfig):
    name = 'striper'
