from django.urls import include, path
from striper.logic import transactions, registration

urlpatterns = [
 
    #Transaction routes
    path('transactions/createstripecharge/', transactions.create_stripe_customer_charge),
    path('transactions/getstripebalance/', transactions.get_stripe_balance),
    path('transactions/stripepayout/', transactions.stripe_payout),
    path('transactions/checkpersonstripedetails/', transactions.check_person_stripe_details),

    #Registration routes
    path('registration/getpersoncards/', registration.get_customer_cards),
    path('registration/addpersoncard/', registration.add_person_card),
    path('registration/createstripeperson/', registration.create_stripe_user),
    path('registration/deleteStripePerson/', registration.delete_stripe_person),
    path('registration/getAllcustomers/', registration.get_all_customers),

    #MPESA routes
    path('', include('MPESA.urls')),

]